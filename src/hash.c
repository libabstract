/*
	hash.c - Hash Tables\Associative Arrays

	Copyright (c) 2007, Matthew Schmidt <matt.alboin@gmail.com>

	Permission to use, copy, modify, and distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include <time.h>

#include "include/alboin.h"
#include "include/ll.h"
#include "include/hash.h"

int fnv_hash(char *data) {
	int i;
	int hash = 2166136261;
	int size = strlen(data);
	
	for(i = 0; i < size; i++) {
		hash = hash * 16777619;
		hash = hash ^ data[i];
	}
	
	return hash;
}

/****************************************/

hash_item *hash_item_new(char *key, void *data, int hashed) {
	hash_item *hi = malloc(sizeof(hash_item));
			
	hi->hash = hashed;	
	hi->key = malloc((strlen(key)*sizeof(char))+1);
	strcpy(hi->key, key);
	
	hi->data = data;	
	return hi;
}


void hash_item_free(hash_item *hi) {
	//free(hi->key);
	free(hi);
}

/****************************************/

hash *hash_new() {
	hash *n = malloc(sizeof(hash));
	
	/* Set up and initial hash. */
	n->collisions = 0;
	n->item_count = 0;
	n->size = HASH_ALLOCATE;
	n->current = 0;
	n->table = calloc(n->size, sizeof(hash_element));
	
	return n;
}

/****************************************/

void hash_set(hash **hp, char *key, void *data) {
	hash *h = *hp;

	/* Update the hash. */
	h->item_count++;

	/* Our hashing of sorts. */
	unsigned int hashed = fnv_hash(key);
	int index = hashed % h->size;
	
	hash_item *hi = hash_item_new(key, data, hashed);	
	
	/* If we're empty. */
	if(h->table[index].type == ELEMENT_OPEN) {	
		/* Now, place it in. */
		h->table[index].type = ELEMENT_PURE;
		h->table[index].data = hi;
	}
	
	/* What is, a collision? No? */
	else if(h->table[index].type == ELEMENT_PURE) {
		/* The current item in purgatory. */
		hash_item *ho = h->table[index].data;
		
		/* If we're what we're looking for. */
		if(hashed == ho->hash)
			ho->data = data;
		
		/* Collision. */
		else {		
			ll *nueve = ll_new();
			h->table[index].data = nueve;
			h->table[index].type = ELEMENT_LL;
		
			ll_add(nueve, ho);
			ll_add(nueve, hi);
		}
	}
	
	/* Hey! Fancy that. We're already a linked list! */
	else if(h->table[index].type == ELEMENT_LL) {
		ll *l = h->table[index].data;
	
		/* All is well, no rehashing needed. */
		if(l->items < MAX_LL_SIZE) {
			ll_add(l, hi);
		}
		
		/* Blah. We need to rehash. */
		else {
			rehash(hp);
			hash_set(hp, key, data);
		}
	}
}

void hash_remove(hash **hp, char *key) {
	hash *h = *hp;

	unsigned int hashed = fnv_hash(key);
	int index = hashed % h->size;
	
	/* Set to empty */
	h->table[index].type = ELEMENT_OPEN;	
}

void *hash_get(hash **hp, char *key) {
	hash *h = *hp;

	int hashed = fnv_hash(key);
	int index = hashed % h->size;
	
	/* If we're being easy. */	
	if(h->table[index].type == ELEMENT_PURE) {
		hash_item *hi = h->table[index].data;
		return hi->data;
	}
	
	/* Mmmm... Crunchy... */
	else if(h->table[index].type == ELEMENT_LL) {
		ll *l = h->table[index].data;
		
		foreach(l) {
			hash_item *hi = l->current->data;
			if(hi->hash == hashed)
				return hi->data;
		}		
	}
}

hash_item *hash_foreach(hash **hp) {
	hash *h = *hp;
	
	while(h->current < h->size) {
		if(h->table[h->current].type == ELEMENT_PURE) {
			hash_item *hi = h->table[h->current].data;
			
			h->current++;
			return hi;
		}
		else if(h->table[h->current].type == ELEMENT_LL) {
			ll  *l = h->table[h->current].data;
			hash_item *hi = ll_foreach(l);
			
			if(hi != NULL)
				return hi;
			else if(hi == NULL)
				h->current++;
		}
		else if(h->table[h->current].type == ELEMENT_OPEN) 
			h->current++;
	}
	
	h->current = 0;
	return NULL;
}

void hash_free(hash **hp) {
	int i;
	hash *h = *hp;
	
	/* Go through and free. */
	hash_item *hi;
	while((hi = hash_foreach(&h)) != NULL)
		hash_item_free(hi);
	
	/* Free everything else. */
	//free(&h->table[0]);
	//free(h);
	
	*hp = NULL;
}

void rehash(hash **hp) {
	int i;
	hash *h = *hp;

	/* Now allocate tmp buffer, which will be what we work on. */
	hash *tmp = hash_new();
	free(tmp->table);
	tmp->size = h->size*2;
	tmp->table = calloc(tmp->size, sizeof(hash_element));

	/* Rehash. */	
	hash_item *hi;
	while((hi = hash_foreach(&h)) != NULL)
		hash_set(&tmp, hi->key, hi->data);

	/* Free the old hash. */
	hash_free(hp);
	
	/* Institute the new one before we leave. */
	*hp = tmp;
}

/****************************************/

#ifdef HASH_DEBUG

char *random_string() {
	int i;
	int size = rand()%32;
	char *ret = malloc(size);
	
	for(i = 0; i < size; i++) 
		ret[i] = 'a' + rand() / (RAND_MAX / ('A' - 'z' + 1) + 1);
		
	ret[size] = '\0';
		
	return ret;	
}

char *int2string(int meow) {
	char *ret = malloc(sizeof(char)*8);
	sprintf(ret, "%i", meow);
	return ret;
}

int main(int argc, char *argv[]) {
	hash *tmp = hash_new(free);
	
	hash_set(&tmp, "Hello", "World!");
	//hash_set(&tmp, "Hello", "World!");
	//int i;
	//for(i = 0; i < 900; i++) {
	//	//hash_set(&tmp, "key", "data");
	//	hash_set(&tmp, int2string(i), random_string());
	//}
	//printf("Item Count: %i\nSize: %i\n", tmp->item_count, tmp->size);
	//printf("Wolf: %s\n", hash_get(&tmp, "Hello"));
	
	//hash_item *dat;
	//int count = 0;
	//while((dat = hash_foreach(&tmp)) != NULL) {
	//	printf("Next: %i[%s : %s]\n",count,  dat->key, dat->data);
	//	free(dat->key);
	//	free(dat->data);
	//	count++;
	//}
	
	ll *l = ll_new();
	ll_add(l, "meow");
	ll_add(l, "mfkd");
	ll_add(l, "XSDFHJ");

	ll_remove(l, 1);
	foreach(l) {
		printf("dfsf: %s\n", l->current->data);
	}

	//hash_remove(&tmp, "Hello");

	//printf("Wolf: %s\n", hash_get(&tmp, "Hello"));
	//hash_free(&tmp);
}

#endif
