/*
	string.c: Unicodes Strings
	
	Copyright (c) 2007, Matthew Schmidt <matt.alboin@gmail.com>

	Permission to use, copy, modify, and distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>;

#include "alboin.h"

typedef int	m_char;
typedef char *	m_string;

#define M_STRING(ms) &ms[5]
#define M_STRING_LENGTH(ms) (int)((char*)ms[1])

const m_char m_utf8_bases[] = {
	0x00, 
	0xc0,
	0xe0,
	0xf0
};

int m_utf8_decode_char_size(char c) {
	int i = 0;

	if(c >= 0x00 && c <= 0x7f)
		i = 1;
	else if(c >= 0xc2 && c <= 0xdf)
		i = 2;
	else if(c >= 0xe0 && c <= 0xef)
		i = 3;
	else if(c >= 0xf0 && c <= 0xf3)
		i = 4;
		
	return i;
}

int m_utf8_encode_char_size(m_char mc) {
	int i = 0;

	if(mc >= 0x000000 && mc <= 0x00007f)
		i = 0;
	else if(mc >= 0x000080 && mc <= 0x0007ff)
		i = 2;
	else if(mc >= 0x000800 && mc <= 0x00ffff)
		i = 3;
	else if(mc >= 0x010000 && mc <= 0x10ffff)
		i = 4;
	
	return i;
}

m_char m_utf8_encode(m_char mc) {
	int size = m_utf8_encode_char_size(mc);
	
	m_char = 
}

int m_string_length(m_string ms) {
	int i;
	int length;
	
	for(i = 0; ms[i] != '\0'; length++)
		i += m_utf8_decode_char_size(ms[i]);
	
	return length;
}

m_string m_string_alloc(char *buff) {
	return calloc(sizeof(char), strlen(buff)+5);
}

m_string m_string_new(char *buff) {
	m_string ret = m_string_alloc(buff);
	
	/* Set the base param's. */
	ret[0] = 0x1c;
	strcpy(M_STRING(ret), buff);
	
	/* Determine the length of the string. */
	
	
	return ret;
}

m_string m_string_copy(m_string ms) {
	m_string ret = malloc(strlen(ms));
	strcpy(ret, ms);
	
	return ret;
}
