/*
	bits.c: Bitwise Operations

	Copyright (c) 2007, Matthew Schmidt <matt.alboin@gmail.com>

	Permission to use, copy, modify, and distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "alboin.h"

unsigned int most_sig(int bits) {
	return (2^bits)/2;
}

int get_max(int bits) {
	return pow(2, bits)-1;
}

int get_higher(int bits) {
	int ret = get_max(bits);
	ret <<= bits;
	return ret;
}

int bit_length(char n) {
	int x;
	if(n > 0) 
		for(x = 0;; n <<= 1, x++) 
			if((n & 128) != 0) 
				return ((sizeof(char)*8) - x);
}
