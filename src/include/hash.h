/*
	hash.h: Hash decelarations.

	Copyright (c) 2007, Matthew Schmidt <matt.alboin@gmail.com>

	Permission to use, copy, modify, and distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#ifndef __HASH_H__
#define __HASH_H__

#define HASH_ALLOCATE	16

#define MAX_LL_SIZE		8	

#define ELEMENT_OPEN	0
#define ELEMENT_LL		1
#define ELEMENT_PURE 	2

typedef struct _hash_item hash_item;
struct _hash_item {
	int hash;
	char *key;
	void *data;
};

typedef struct _hash_element hash_element;
struct _hash_element {
	int type;
	void *data;
};

typedef struct _hash hash;
struct _hash {
	int size;
	int item_count;
	int collisions;
	int current;
	hash_element *table;
};

void rehash(hash **hp);
int fnv_hash(char *data);
void hash_set(hash **hp, char *key, void *data);
void *hash_get(hash **hp, char *key);
hash *hash_new();

#endif
