/*
	Poly item declarations.

	Copyright (c) 2007, Matthew Schmidt <matt.alboin@gmail.com>

	Permission to use, copy, modify, and distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#ifndef POLY_H
#define POLY_H

union types {
	void *_ptr;
	char _char;
	char *_string;
	short _short;
	int _int;
	long _long;
	long long _long_long;
	float _float;
	double _double;
};

typedef struct poly poly;

struct poly {
	char type;
	union types data;
};

#define M_PTR		0
#define M_CHAR		1
#define M_STRING 	2
#define M_SHORT		3
#define M_INT		4
#define M_LONG		5
#define M_LONG_LONG	6
#define M_FLOAT		7
#define M_DOUBLE	8

#endif
