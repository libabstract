/*
	poly.c: Polymorphic Data Types

	Copyright (c) 2007, Matthew Schmidt <matt.alboin@gmail.com>

	Permission to use, copy, modify, and distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include <time.h>

#include "alboin.h"
#include "ll.h"
#include "hash.h"
#include "poly.h"

poly m_poly_new(char type, ...) {
	va_list list;
	poly ret;

	/* Set initial. */
	ret.type = type;

	va_start(list, type);
	switch(type) {
		case M_PTR:
			ret._ptr = va_arg(list, void*);
			break;
		case M_CHAR:
			ret._char = va_arg(list, char);
			break;
		case M_STRING:
			ret._string = va_arg(list, char*);
			break;
		case M_SHORT:
			ret._string = va_arg(list, short);
			break;
		case M_INT:
			ret._int = va_arg(list, int);
			break;
		case M_LONG:
			ret._long = va_arg(list, long);
			break;
		case M_LONG_LONG:
			ret._long_long = va_arg(list, long long);
			break;
		case M_FLOAT:
			ret._float = va_arg(list, float);
			break;
		case M_DOUBLE:
			ret._double = va_arg(list, double);
			break;
	}

	return ret;
}

