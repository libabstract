/*
	ll.c - Linked Lists

	Copyright (c) 2007, Matthew Schmidt <matt.alboin@gmail.com>

	Permission to use, copy, modify, and distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>

#include "include/alboin.h"
#include "include/ll.h"

ll *ll_new() {
	ll *l = malloc(sizeof(ll));

	l->items = 0;
	l->fe_started = false;
	l->next_state = true;
	l->base = malloc(sizeof(linked_list));
	l->current = l->base;
	
	return l;
}

void ll_add(ll *l, void *a) {
	l->items++;

	l->current->data = a;
	
	l->current->next = malloc(sizeof(ll));
	l->current->next->parent = l->current;
	l->current = l->current->next;
	l->current->next = NULL;
}

void *ll_remove(ll *l, int item) {
	int count = 0;
	foreach(l) {
		if(count == (item-1)) {
			linked_list *rem = l->current->next;
			l->current->next = rem->next;
			free(rem);
			l->items--;
			break;
		}
		count++;
	}
}

void ll_free(ll *l) {
	l->current = l->base;
	while(1) {
		/* Free the structure. */
		if(l->current->next == NULL) {
			free(l->current);
			break;
		}
		else {
			linked_list *tmp = l->current->next;	
			free(l->current);
			l->current = tmp;
		}	
	}
}

void ll_switch(linked_list *l1, linked_list *l2) {
	void *tmp;

	tmp = l1->data;
	l1->data = l2->data;
	l2->data = tmp;
}

void ll_sort(ll *l, int (*comp)(void*, void*)) {
	int t = false;
	
	while(1) {
		t = false;
		l->current = l->base;
		
		foreach(l) {
			int r = comp(l->current->data, l->current->next->data);
			if(r == 1) {
				ll_switch(l->current, l->current->next);
				t = true;
			}
		}
		
		if(!t)
			break;
	}
}

void ll_reset(ll *l) {
	l->current = l->base;
}

void *ll_foreach(ll *l) {
	void *ret;

	/* We have to reset if we haven't already. */
	if(!l->fe_started) {
		ll_reset(l);
		l->fe_started = true;
	}
		
	if(!l->next_state)
		return NULL;
	
	ret = l->current->data;
		
	if(l->current->next != NULL) {
		l->current = l->current->next;
		l->next_state = true;
	}
	
	/* We're finished here. Wrap it all up. */
	else {
		l->fe_started = false;
		l->next_state = false;
	}
		
	return ret;
}

