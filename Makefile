objects = src/ll.o src/hash.o src/string.o src/bits.o src/gc.o

all : ${objects}
	gcc -o ui ${objects}

%.o : %.c
	gcc -c $< -o $@